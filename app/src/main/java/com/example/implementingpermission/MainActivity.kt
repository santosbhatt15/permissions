package com.example.implementingpermission

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.implementingpermission.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val TAG: String = "AppDebug"

    private val getImageFromGallery = registerForActivityResult(
        ActivityResultContracts.GetContent()
    ) {
        binding.ivImageFromGallery.setImageURI(it)
    }

    private val getImageFromCamera = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ){
        if (it.resultCode == Activity.RESULT_OK) {
            val imageBitmap = it.data?.extras?.get("data") as Bitmap
            binding.ivImageFromGallery.setImageBitmap(imageBitmap)
        }else{
            Toast.makeText(this, "Can't Open Camera", Toast.LENGTH_SHORT).show()
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.btnPickImageFromGallery.setOnClickListener {
            if(ContextCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED
            )
            {
                getImageFromGallery.launch("image/*")
            }else{
                ActivityCompat.requestPermissions(this,
                    arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                    1
                )
            }
        }

        binding.btnCameraPicker.setOnClickListener {
            if (ContextCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.CAMERA
                )==PackageManager.PERMISSION_GRANTED)
            {
                getImageFromCamera.launch(Intent(MediaStore.ACTION_IMAGE_CAPTURE))
            }else{
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(android.Manifest.permission.CAMERA),
                    2
                )
            }
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            1->
                if(grantResults[0]==PackageManager.PERMISSION_GRANTED)
                {
                    getImageFromGallery.launch("image/*")
                } else
                {
                    Toast.makeText(this, "Permission required to access gallery",
                        Toast.LENGTH_SHORT).show()
                }
            2->
                if(grantResults[0]==PackageManager.PERMISSION_GRANTED)
                {
                    getImageFromCamera.launch(Intent(MediaStore.ACTION_IMAGE_CAPTURE))
                }else
                {
                    Toast.makeText(this, "Permission required to access camera",
                        Toast.LENGTH_SHORT).show()
                }
        }
    }
}